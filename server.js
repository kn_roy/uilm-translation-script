const MongoClient = require('mongodb').MongoClient;
const fs = require('fs');

const uri = "connection string";
const dbName = "your database name";
const collectionName = "your collection name";

const client = new MongoClient(uri);
let isConnected = false;

let translationData = {};
let missingKeys = [];

let database = async () => {
    try {
        if (!isConnected) {
            await client.connect();
            isConnected = true;
        }
        return client.db(dbName);
    } catch (e) {
        return null;
    }
}

let translationDataModify = (data) => {
    let trans = {};
    data.Translations.forEach(item => {
        trans[item.KEY] = {
            en: item.English,
            de: item.German,
            fr: item.French,
            it: item.Italian
        }
    })
    return trans;
}

let readTranslationsFromJson = () => {
    try {
        let data = String(fs.readFileSync('translation.json'));
        let parsedData = JSON.parse(data);
        translationData = translationDataModify(parsedData);
    } catch (e) {
        return [];
    }
}

let writeMissingKeys = () => {

    let entries = [];

    let getValueByLang = (res, resType) => {
        let data = res.filter(item => item.Culture === resType);
        let value = data[0].Value;
        return value.replace(/(\r\n|\n|\r)/gm, "");
    }

    try {
        entries.push("KEY,SL,English,German,French,Italian");

        missingKeys.forEach((item, index) => {
            let data = [];
            data.push(item.KeyName);
            data.push(index + 1);
            if (item.Resources) {
                data.push(getValueByLang(item.Resources, 'en'));
                data.push(getValueByLang(item.Resources, 'de'));
                data.push(getValueByLang(item.Resources, 'fr'));
                data.push(getValueByLang(item.Resources, 'it'));
                entries.push(data.join(","));
            }
        })
    } catch (e) {
        console.log(e);
    }

    fs.writeFileSync('missing_translations.csv',
        entries.join("\n"));
}


let cloneFromResource = (data) => {
    let lang = translationData[data.KeyName];
    if (!lang)
        return null;
    return {
        AppId: data.AppId,
        KeyName: data.KeyName,
        Resources: [
            {Value: lang.en, Culture: 'en'},
            {Value: lang.de, Culture: 'de'},
            {Value: lang.it, Culture: 'it'},
            {Value: lang.fr, Culture: 'fr'}
        ],
        MirrorText: data.MirrorText,
        Context: data.Context || null,
        Type: data.Type || null
    }
}

let updateOne = async (collection, data) => {
    try {
        let clonedData = cloneFromResource(data);
        if (!clonedData) {
            missingKeys.push(data);
            return;
        }

        let query = {_id: data._id};
        let options = {upsert: true}
        let value = {
            $set: clonedData
        }
        await collection.updateOne(query, value, options);
        console.log('Updated: ', data.KeyName);
    } catch (e) {

    }
}

async function run() {
    try {

        readTranslationsFromJson();

        let db = await database();
        const uilm = db.collection(collectionName)
        //const cursor = await uilm.findOne();
        const cursor = await uilm.find();
        //console.log(cursor);
        await cursor.forEach((data) => updateOne(uilm, data));
    } catch (e) {
        console.log(e);
    } finally {
        writeMissingKeys();
        await client.close();
    }
}

run().catch(console.log);
